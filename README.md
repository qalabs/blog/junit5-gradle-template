# JUnit5 Gradle Template

- Basic JUnit 5 Gradle Template
- Requires Java 11. Change `sourceCompatibility` property in `build.gradle` to change supported Java version

## Cloning the repository

    git clone https://gitlab.com/qalabs/blog/junit5-gradle-template.git
    
## Run the tests

    gradlew clean test

## See also

- https://blog.qalabs.pl/narzedzia/git-cmder/
- https://blog.qalabs.pl/narzedzia/selenium-przegladarki/
- https://blog.qalabs.pl/java/przygotowanie-srodowiska/
